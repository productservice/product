package main

import (
	"github/FIrstService/template-service/product-service/product/config"
	pb "github/FIrstService/template-service/product-service/product/genproto/product"
	"github/FIrstService/template-service/product-service/product/pkg/db"
	"github/FIrstService/template-service/product-service/product/pkg/logger"
	"github/FIrstService/template-service/product-service/product/service"

	grpcclient "github/FIrstService/template-service/product-service/product/service/grpc_client"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "Product")
	defer logger.Cleanup(log)

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	connDB, err := db.ConnectTODB(cfg)
	if err != nil {
		log.Fatal("sqlx connection to postgres error", logger.Error(err))
	}

	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("error while connect to clients", logger.Error(err))
	}

	productService := service.NewProductService(grpcClient, connDB, log)

	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	pb.RegisterProductServiceServer(s, productService)
	log.Info("main: server runnig",
		logger.String("port", cfg.RPCPort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}
