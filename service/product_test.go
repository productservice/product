package service

import (
	"context"
	pb "github/FIrstService/template-service/product-service/product/genproto/product"
	l "github/FIrstService/template-service/product-service/product/pkg/logger"
	grpcclient "github/FIrstService/template-service/product-service/product/service/grpc_client"
	"github/FIrstService/template-service/product-service/product/storage"
	"reflect"
	"testing"
)

func Product_CreateProduct(t *testing.T) {
	type fields struct {
		store   *grpcclient.ServiceManager
		storage storage.IStorage
		logger  l.Logger
	}
	type args struct {
		ctx context.Context
		req *pb.ProductFullInfo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *pb.ProductFullInfoResponse
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &ProductService{
				store:   tt.fields.store,
				storage: tt.fields.storage,
				logger:  tt.fields.logger,
			}
			got, err := s.CreateProduct(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("ProductService.CreateProduct() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ProductService.CreateProduct() = %v, want %v", got, tt.want)
			}
		})
	}
}
