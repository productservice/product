package repo

import (
	pb "github/FIrstService/template-service/product-service/product/genproto/product"
)

// ProductStorageI ...
type ProductStorageI interface {
	CreateProduct(*pb.ProductFullInfo) (*pb.ProductFullInfoResponse, error)
	CreateCategory(*pb.Category) (*pb.Category, error)
	CreateType(*pb.Type) (*pb.Type, error)
	GetProductInfoByid(*pb.Ids) (*pb.ProductFullInfoResponse, error)
	DeleteInfo(int) error
	UpdateByid(*pb.Product) (*pb.Product, error)
}
